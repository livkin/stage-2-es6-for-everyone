import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  // todo: show fighter info (image, name, health, etc.)

  if (fighter) {
    fighterElement.appendChild(createFighterInfoView(fighter, position));
  }

  return fighterElement;
}

function createFighterInfoView(fighterMap, position) {

  const fighter = {};
  for (const [key, value] of fighterMap) {
    fighter[key] = value;
  }

  const list = createElement({ tagName: 'ul' });
  const image = createFighterPreviewImage(fighter);
  
  if (position === 'right') {
    image.style.transform = 'scale(-1, 1)';
  }
  const imageItem = createElement({ tags: 'li' });
  imageItem.appendChild(image);
  list.appendChild(imageItem);

  const fieldsToShow = ['health', 'attack', 'defense'];
  fieldsToShow.forEach(fieldName => {
    const fighterItem = createElement('li');
    fighterItem.innerHTML = `<p>${fieldName}: ${fighter[fieldName]}</p>`;
    fighterItem.style.color = 'white';
    list.appendChild(fighterItem);
  });

  const fighterInfoView = createElement({
    tagName: 'div', // className: 'preview-container___versus-img'
  });
  fighterInfoView.appendChild(list);

 // fighterInfoView.style.width = '100px';

  return fighterInfoView;

}

export function createFighterPreviewImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name
  };
  const imgElement = createElement({
    tagName: 'img',
    // todo: use image height, because Bison is too wide
    className: 'preview-container___versus-img',
    attributes,
  });

  return imgElement;
}


export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
