import { controls } from '../../constants/controls';

class Controller {

  constructor(leftFighter, rightFighter) {

    this.leftFighter = leftFighter;
    this.rightFighter = rightFighter;
    this.controls = controls;
    this.pressedKeys = [];

  }

  whatToDo() {
    // test: todo: one per 10 sec
    if ((Date.now() - this.leftFighter.lastCritTime) > 10 * 1000) {
      const p1Crit = this.p1CritPressed();
      if (p1Crit) {
        this.rightFighter.damage(this.leftFighter.attack * 2);
        this.leftFighter.lastCritTime = Date.now();
      }
    }

    if ((Date.now() - this.rightFighter.lastCritTime) > 10 * 1000) {
      const p2Crit = this.p2CritPressed();
      if (p2Crit) {
        this.leftFighter.damage(this.rightFighter.attack * 2);
        this.rightFighter.lastCritTime = Date.now();
      }
    }

    const p1Block = this.p1BlockPressed();
    const p2Block = this.p2BlockPressed();

    const p1Attack = this.p1AttackPressed();
    const p2Attack = this.p2AttackPressed();

    if (!p1Block && !p2Block && p1Attack) {
      const hitPower = this.leftFighter.hitPower;
      const blockPower = this.rightFighter.blockPower;
      this.rightFighter.damage(hitPower - blockPower);
    }

    if (!p2Block && !p1Block && p2Attack) {
      const hitPower = this.rightFighter.hitPower;
      const blockPower = this.leftFighter.blockPower;
      this.leftFighter.damage(hitPower - blockPower);
    }
  }

  p1CritPressed() {
    const pCrit = this.controls.PlayerOneCriticalHitCombination
      .every(item => !!this.pressedKeys[item]);

    return pCrit;
  }

  p2CritPressed(pressedKeys) {
    const pCrit = this.controls.PlayerTwoCriticalHitCombination
      .every(item => !!this.pressedKeys[item]);

    return pCrit;
  }

  p1BlockPressed() {
    const pBlock = !!this.pressedKeys[this.controls.PlayerOneBlock];

    return pBlock;
  }

  p2BlockPressed() {
    const pBlock = !!this.pressedKeys[this.controls.PlayerTwoBlock];

    return pBlock;
  }

  p1AttackPressed() {
    const pAttack = !!this.pressedKeys[this.controls.PlayerOneAttack];

    return pAttack;
  }
  p2AttackPressed() {
    const pAttack = !!this.pressedKeys[this.controls.PlayerTwoAttack];

    return pAttack;
  }

}

export default Controller