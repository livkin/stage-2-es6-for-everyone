import { createElement } from '../helpers/domHelper';
import { createFighterImage } from './fighterPreview';
import Fighter from './fighter';
import Controller from './controller';
import { showWinnerModal } from './modal/winner';


export function renderArena(selectedFighters) {
  const root = document.getElementById('root');
  const arena = createArena(selectedFighters);

  root.innerHTML = '';
  root.append(arena);

  // todo:
  // - start the fight
  // - when fight is finished show winner

  const leftFighterIndicator = document.querySelector('#left-fighter-indicator');
  const rightFighterIndicator = document.querySelector('#right-fighter-indicator');

  const leftFighter = new Fighter(selectedFighters[0]);
  const rightFighter = new Fighter(selectedFighters[1]);
 
  leftFighterIndicator.style.width = `${leftFighter.healthPercent}%`;
  rightFighterIndicator.style.width = `${rightFighter.healthPercent}%`;

  const controller = new Controller(leftFighter, rightFighter);

  document.addEventListener('keydown', function (event) {
    return calculateKeyDown(event, controller)
  });

  document.addEventListener('keyup', function (event) {
    delete controller.pressedKeys[event.code];
  });

  // fight().then(result => alert(result));

}

function calculateKeyDown(event, controller) {

  const leftFighterIndicator = document.querySelector('#left-fighter-indicator');
  const rightFighterIndicator = document.querySelector('#right-fighter-indicator');

  controller.pressedKeys[event.code] = true;

  controller.whatToDo();

  leftFighterIndicator.style.width = `${controller.leftFighter.healthPercent}%`;
  rightFighterIndicator.style.width = `${controller.rightFighter.healthPercent}%`;


  if (rightFighterIndicator.style.width === '0%') {
    // alert('left win');
    showWinnerModal(controller.leftFighter);
  }
  if (leftFighterIndicator.style.width === '0%') {
    // alert('right win');
    showWinnerModal(controller.rightFighter);
  }
}

function getHitPower(attack) {
  const criticalHitChance = Math.random() + 1;

  return attack * criticalHitChance;
}

function getBlockPower(defense) {
  const dodgeChance = Math.random() + 1;

  return defense * dodgeChance;
}

function fight() {
  return new Promise((resolve, reject) => {
    
    setInterval(whoIsWinner(resolve, reject), 500);

  });
}

function createArena(selectedFighters) {
  const arena = createElement({ tagName: 'div', className: 'arena___root' });
  const healthIndicators = createHealthIndicators(...selectedFighters);
  const fighters = createFighters(...selectedFighters);

  arena.append(healthIndicators, fighters);
  return arena;
}

function createHealthIndicators(leftFighter, rightFighter) {
  const healthIndicators = createElement({ tagName: 'div', className: 'arena___fight-status' });
  const versusSign = createElement({ tagName: 'div', className: 'arena___versus-sign' });
  const leftFighterIndicator = createHealthIndicator(leftFighter, 'left');
  const rightFighterIndicator = createHealthIndicator(rightFighter, 'right');

  healthIndicators.append(leftFighterIndicator, versusSign, rightFighterIndicator);
  return healthIndicators;
}

function createHealthIndicator(fighter, position) {
  const { name } = fighter;
  const container = createElement({ tagName: 'div', className: 'arena___fighter-indicator' });
  const fighterName = createElement({ tagName: 'span', className: 'arena___fighter-name' });
  const indicator = createElement({ tagName: 'div', className: 'arena___health-indicator' });
  const bar = createElement({ tagName: 'div', className: 'arena___health-bar', attributes: { id: `${position}-fighter-indicator` } });

  fighterName.innerText = name;
  indicator.append(bar);
  container.append(fighterName, indicator);

  return container;
}

function createFighters(firstFighter, secondFighter) {
  const battleField = createElement({ tagName: 'div', className: `arena___battlefield` });
  const firstFighterElement = createFighter(firstFighter, 'left');
  const secondFighterElement = createFighter(secondFighter, 'right');

  battleField.append(firstFighterElement, secondFighterElement);
  return battleField;
}

function createFighter(fighter, position) {
  const imgElement = createFighterImage(fighter);
  const positionClassName = position === 'right' ? 'arena___right-fighter' : 'arena___left-fighter';
  const fighterElement = createElement({
    tagName: 'div',
    className: `arena___fighter ${positionClassName}`,
  });

  fighterElement.append(imgElement);
  return fighterElement;
}
