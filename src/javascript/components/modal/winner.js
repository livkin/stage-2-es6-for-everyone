import { showModal } from './modal';
import { createElement } from '../../helpers/domHelper';

export function showWinnerModal(fighter) {
  // call showModal function 

  const bodyElement = createImage(fighter);

  showModal({
    title: `${fighter.name} win!`,
    bodyElement,
    onClose: restart,
  });
}

function restart() {
  document.location.reload();
}

function createImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source,
    title: name,
    alt: name, 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter___fighter-image',
    attributes
  });

  return imgElement;
}

