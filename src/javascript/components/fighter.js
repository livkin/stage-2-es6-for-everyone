
class Fighter{
  
  constructor(fighterProps) {
    
    this._id = fighterProps._id;
    this.name = fighterProps.name;
    this.source = fighterProps.source;
    
    this.attack = Number.parseInt(fighterProps.attack);
    this.defense = Number.parseInt(fighterProps.defense);
    this.health = Number.parseInt(fighterProps.health);
    
    this.lastCritTime = 0;

    this.healthMax = this.health;
    
  }

  damage(value) {
    this.health = Math.max(0, this.health - Math.max(0, value));
  }

  get healthPercent() {
    return 100/this.healthMax * this.health;
  }  

  get hitPower() {
    const criticalHitChance = Math.random() + 1;
  
    return this.attack * criticalHitChance;
  }

  get blockPower() {
    const dodgeChance = Math.random() + 1;
  
    return this.defense * dodgeChance;
  }

}

export default Fighter